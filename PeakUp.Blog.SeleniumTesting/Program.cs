﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System.Linq;
using System.Threading;

namespace PeakUp.Blog.SeleniumTesting
{
    public class ITest
    {
        public IWebDriver driver { get; set; }
        public const string url = "https://www.google.com.tr/";
        public ITest(IWebDriver _driver)
        {
            driver = _driver;
        }


        /// <summary>
        /// Open the page a long with a brand new cookies, clean history
        /// </summary>
        /// <returns></returns>
        public bool OpenPage()
        {
            bool result = false;

            driver.Navigate().GoToUrl(url);

            if (driver.Title.Contains("Google") == true)
            {
                result = true;
            }
            return result;
        }


        /// <summary>
        /// Find the input from <input name=""></input>
        /// </summary>
        /// <param name="inputId"></param>
        /// <returns></returns>
        public IWebElement GetInput(string inputName)
        {
            IWebElement result = null;

            result = driver.FindElements(By.Name(inputName)).Any() ? driver.FindElements(By.Name(inputName)).SingleOrDefault() : null;

            return result;
        }


        /// <summary>
        /// Find and fill the input with necessary content!
        /// </summary>
        /// <param name="inputId"></param>
        /// <param name="inputContent">String content for typing in to the input element</param>
        public void SendKeys(string inputName, string inputContent)
        {
            IWebElement element = GetInput(inputName);

            foreach (var item in inputContent)
            {
                element.SendKeys(item.ToString());
            }


            element.SendKeys(Keys.Enter);
        }


        /// <summary>
        /// Loop trough in the elements and find the spesific linkHref
        /// </summary>
        /// <param name="linkHref"></param>
        /// <returns></returns>
        public IWebElement GetLink(string linkHref)
        {
            IWebElement result = null;

            foreach (var item in driver.FindElements(By.TagName("link")))
            {
                if (item.GetAttribute("href").ToString().Contains(linkHref))
                {
                    result = item;

                    Wait(4000);
                    
                    break;
                }
            }

            return result;
        }


        /// <summary>
        /// Due slow internet and page rendering wait for spesific miliseconds for everystep
        /// </summary>
        /// <param name="miliSeconds"></param>
        public void Wait(int miliSeconds)
        {
            Thread.Sleep(miliSeconds);
        }


        /// <summary>
        /// Close and kill the session with browser!
        /// </summary>
        public void Close()
        {
            driver.Close();
            driver.Quit();
        }


        public string RunTest()
        {
            string result = string.Empty;


            if (OpenPage() == false)
            {
                result = "Page couldn't open mate!";
            }

            Wait(5000);

            if (GetInput("q") != null)
            {
                Wait(5000);

                SendKeys("q", "peakup.org");

                Wait(5000);

                if (GetLink("peakup.org") != null)
                {
                    Wait(5000);
                    result = "";
                    Close();
                }
            }

            return result;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {

            IWebDriver chrome = new ChromeDriver();

            ITest doesPeakUpShowUpInGoogle_ChromeBrowser = new ITest(chrome);

            if (string.IsNullOrWhiteSpace(doesPeakUpShowUpInGoogle_ChromeBrowser.RunTest()))
            {
                System.Console.WriteLine("Chrome test passed!");
            }


            IWebDriver firefox = new FirefoxDriver();
            ITest doesPeakUpShowUpInGoogle_Firefox = new ITest(firefox);

            if (string.IsNullOrWhiteSpace(doesPeakUpShowUpInGoogle_Firefox.RunTest()))
            {
                System.Console.WriteLine("firefox test passed!");
            }
        }
    }
}
